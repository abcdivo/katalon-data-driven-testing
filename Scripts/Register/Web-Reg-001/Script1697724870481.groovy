import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar')

WebUI.setText(findTestObject('Object Repository/Register/set-text_Nama'), 'Ridho')

WebUI.setText(findTestObject('Object Repository/Register/input_Tanggal lahir_birth_date'), '14-Jul-2000')

WebUI.setText(findTestObject('Object Repository/Register/set-text_E-Mail'), 'asalajadulu@gmail.com')

WebUI.setText(findTestObject('Object Repository/Register/set-text_Whatsapp'), '081273191432')

WebUI.setText(findTestObject('Object Repository/Register/set-text_Kata Sandi-register'), 'sigem123')

WebUI.setText(findTestObject('Object Repository/Register/set-text_Konfirmasi kata sandi-register'), 'sigem123')

WebUI.click(findTestObject('Object Repository/Register/checkbox_register'))

WebUI.click(findTestObject('Object Repository/Register/button_Daftar'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Register/span_Verifikasi Email'))

WebUI.closeBrowser()

